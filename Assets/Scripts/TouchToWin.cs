﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchToWin : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            GameManager.GMComp.Win();
        }
    }
}
