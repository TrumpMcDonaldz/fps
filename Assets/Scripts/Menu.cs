﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject Instruction;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;

        if (Instruction != default)
        {
            StartCoroutine(Instruction.GetComponent<PlayVid>().Instructions());
        }
    }

    public void Play()
    {
        SceneManager.LoadScene("Game");
    }

    public void Instructions()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void Quit()
    {
        Application.Quit(); 
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
