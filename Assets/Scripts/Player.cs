﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour, IDamageable
{
    //Inspector Variables

    public float XSens, YSens;

    public float _Speed;

    public Rigidbody RB;

    public Build Build;

    public Shotgun SG;

    public int Resource;

    public Text ResourceText, ModeText, HealthText;

    public bool IsEditing;

    public LayerMask BuildingLM;

    public Image SGCross, BuildCross, EditCross;

    //Interface Variables

    bool IsMoving { get; set; }

    float Speed { get; set; }
    public Vector2 Health { get; set; }

    //Private Variables

    float MouseX, MouseY;

    private void Awake()
    {
        Health = new Vector2(100, 100);

        ModeText.text = "Shoot Mode";

        Speed = 10;

        Build = this.transform.GetComponentInChildren<Build>();

        SG = this.transform.GetComponentInChildren<Shotgun>();

        Build.gameObject.SetActive(false);

        UpdateResource(0);

        TakeDamage(0);
    }

    void Update()
    {
        Select();

        Movement();
    }

    private void Select() // Lazy hard coded shit, will expand when there's time...
    {
        if (IsEditing)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            BuildCross.enabled = false;

            SGCross.enabled = false;

            EditCross.enabled = true;

            ModeText.text = "Edit Mode";

            Build.gameObject.SetActive(false);

            SG.gameObject.SetActive(false);

            StartCoroutine(EditMode());

            return;
        }

        if (!Input.GetKeyDown(KeyCode.Q))
        {
            return;
        }


        if (Build.gameObject.activeInHierarchy)
        {
            ModeText.text = "Shoot Mode";

            Build.gameObject.SetActive(false);

            SG.gameObject.SetActive(true);

            BuildCross.enabled = false;

            SGCross.enabled = true;

            EditCross.enabled = false;

            return;
        }

        ModeText.text = "Build Mode";

        Build.gameObject.SetActive(true);

        SG.gameObject.SetActive(false);

        BuildCross.enabled = true;

        SGCross.enabled = false;

        EditCross.enabled = false;
    }

    private void Start()
    {
        IsMoving = false;

        Speed = 5;
    }

    //Movement

    void Movement()
    {
        if (this.transform.position.y < -10)
        {
            GameManager.GMComp.Lose();

            Destroy(this.gameObject, 2);
        }

        print("Movement");

        //Speed = _Speed;

        _Movement(this.transform.TransformDirection(new Vector3(Input.GetAxisRaw("Horizontal") * Speed, RB.velocity.y, Input.GetAxisRaw("Vertical") * Speed)));

        Rotate();
    }

    void _Movement(Vector3 Destination)
    {
        print(IsMoving);

        RB.velocity = Destination;
    }

    void Rotate()
    {
        Cursor.lockState = CursorLockMode.Locked;

        MouseX += Input.GetAxisRaw("Mouse X") * Time.deltaTime * XSens;

        MouseY += Input.GetAxisRaw("Mouse Y") * Time.deltaTime * YSens;

        //Clamp Y to -90 and 90

        transform.rotation = Quaternion.Euler(0, MouseX, 0);

        Camera.main.transform.localRotation = Quaternion.Euler(Mathf.Clamp(-MouseY, -90, 90), Camera.main.transform.localRotation.y, 0);
    }

    public void UpdateResource(int _Resource)
    {
        Resource += _Resource;

        if (ResourceText != default)
        {
            ResourceText.text = Resource.ToString();
        }
    }

    public IEnumerator EditMode()
    {
        if (IsEditing)
        {
            yield break;
        }

        IsEditing = true;

        var Cam = Camera.main;

        var EditedObjs = new List<GameObject>();

        yield return null;

        while (!Input.GetKeyDown(KeyCode.G))
        {
            yield return null;

            if (Input.GetMouseButtonDown(1))
            {
               foreach (var G in EditedObjs)
                {
                    G.GetComponent<MeshRenderer>().material.color = Color.cyan;
                }

                EditedObjs.Clear();

                continue;
            }

            if (!Input.GetMouseButtonDown(0) && !Input.GetMouseButton(0))
            {
                continue;
            }

            if (!Physics.Raycast(Cam.ScreenPointToRay(Input.mousePosition), out RaycastHit Hit, 6f, BuildingLM))
            {
                continue;
            }

            if (Hit.collider.GetComponent<Support>() != default)
            {
                continue;
            }

            var GO = Hit.collider.gameObject;


            if (!EditedObjs.Contains(GO))
            {
                GO.GetComponent<MeshRenderer>().material.color = new Color(Color.blue.r, Color.blue.g, Color.blue.b, 0.5f);

                EditedObjs.Add(GO);
            }
        }

        EditedObjs.RemoveAll(x => x == default);

        foreach (var G in EditedObjs)
        {
            UpdateResource(G.GetComponent<Build>().ResourceCost);

            Destroy(G);
        }

        SG.gameObject.SetActive(true);

        IsEditing = false;

        EditCross.enabled = false;

        SGCross.enabled = true;

        ModeText.text = "Shoot Mode";

        SG.CanFire = true;
    }

    public void TakeDamage(float Damage)
    {
        Health = new Vector2(Health.x - Damage, Health.y);

        if (Health.x <= 0)
        {
            Health = new Vector2(0, Health.y);

            GameManager.GMComp.Lose();

            Destroy(this.gameObject, 2);
        }

        HealthText.text = $"{Health.x} / {Health.y}";
    }

    private void OnDestroy()
    {
        SceneManager.LoadScene("Menu");
    }
}
