﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    //Variables

    public static GameObject GM, Canvas, Player, Terrain;

    public static GameManager GMComp;

    public static int Kills, KillsToWin;

    public Text KillCount;

    public GameObject WinP, LoseP;

    public NavMeshSurface NMS;

    private void Awake()
    {
        Application.targetFrameRate = -1;

        KillsToWin = 5;

        GM = this.gameObject;

        GMComp = this;

        Player = GameObject.Find("Player");

        Canvas = GameObject.Find("Canvas");

        Terrain = GameObject.Find("Terrain");

        UpdateKills(0);
    }

    public void UpdateKills(int AddedKills)
    {
        Kills += AddedKills;

        if (KillCount != default)
        {
            KillCount.text = Kills.ToString();
        }
    }

    public void UpdateNavMesh()
    {
        NMS.UpdateNavMesh(NMS.navMeshData);
    }

    public void Win()
    {
        //Win

        if (WinP != default)
        {
            WinP.SetActive(true);

            Destroy(LoseP);

            StartCoroutine(WinP.GetComponent<PlayVid>().Win());
        }
    }

    public void Lose()
    {
        if (LoseP != default)
        {
            LoseP.SetActive(true);
        }
    }
}
