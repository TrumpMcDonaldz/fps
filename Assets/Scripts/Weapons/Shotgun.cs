﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Shotgun : Weapon
{
    public int PCount;

    public float PRad;

    public Image Crosshair;

    public GameObject PelletVisualPrefab;

    public bool Scatter;

    public List<Vector2> PelletBloom(RectTransform CrosshairRT, int PelletCount, float PelletRad)
    {
        Rect R = CrosshairRT.rect;

        List<Vector2> PelletPos = new List<Vector2>();

        //If PelletSize > Crosshair

        if (R.width < PelletRad * PCount || R.height < PelletRad * PCount)
        {
            for (int Pellet = 1; Pellet <= PelletCount; Pellet++)
            {
                PelletPos.Add(R.center);
            }

            return PelletPos;
        }

        //Bloom

        for (int Pellet = 1; Pellet <= PelletCount; Pellet++)
        {
            var P = new Vector2(Random.Range(R.xMin, R.xMax), Random.Range(R.yMin, R.yMax));

            //Make sure Pellets do not overlap.

            if (PelletPos.Any(x => Mathf.Abs(Vector2.Distance(P, x)) < PelletRad))
            {
                --Pellet;

                continue;
            }

            //P = Vector2.zero;

            PelletPos.Add(P);
        }

        return PelletPos;
    }

    public override void Shoot()
    {
        if (!Input.GetMouseButtonDown(0) || !CanFire || GameManager.Player.GetComponent<Player>().Resource - ResourceCost < 0)
        {
            return;
        }

        CanFire = false;

        var Cam = Camera.main;

        //var Ray = Cam.ScreenPointToRay(Cam.ViewportToScreenPoint(new Vector3(0.5f, 0.5f, 0)));

        List<Vector2> PelletPos = PelletBloom(Crosshair.rectTransform, PCount, PRad);

        foreach (Vector2 Point in PelletPos)
        {
            //print($"MOUSEPOS {Input.mousePosition}");

            var Offset = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            var Ray = Cam.ScreenPointToRay(Offset + Point);

            var Dir = (Scatter) ? Ray.direction : Camera.main.transform.TransformDirection(Vector3.forward);

            Debug.DrawRay(Ray.origin, Dir * 1000, Color.black);

            //Miss

            if (!Physics.SphereCast(Ray.origin, PRad, Dir, out RaycastHit Hit, Mathf.Infinity, TargetLM))
            {
                continue;
            }

            print("Shoot");

            if (Hit.collider.GetComponent<Support>() != default)
            {
                continue;
            }

            //Deal Damage

            IDamageable ID = Hit.collider.gameObject.GetComponent<IDamageable>();

            if (ID == default)
            {
                continue;
            }

            ID.TakeDamage(Damage / PCount);

            //Show Pellet Hit Visual

            var PHV = Instantiate(PelletVisualPrefab);

            PHV.transform.SetParent(GameManager.Canvas.transform, false);

            PHV.transform.localPosition = Point;

            //print($"On The Rise {PHV.GetComponent<RectTransform>().anchoredPosition}");

            Destroy(PHV, 0.2f);
        }

        StartCoroutine(FireCost());
    }

    private void Update()
    {
        Shoot();
    }
}
