﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public float EquipTime, CoolDown, Damage, ClipSize;

    public int ResourceCost;

    public bool CanFire;

    public LayerMask TargetLM;

    public void Awake()
    {
        CanFire = true;
    }

    public virtual void Shoot()
    {

    }

    public IEnumerator FireCost(bool InstantCoolDown = false, bool FreeCost = false)
    {
        if (InstantCoolDown)
        {
            CanFire = true;
        }

        yield return new WaitForSeconds(CoolDown);

        CanFire = true;

        if (!FreeCost)
        {
            GameManager.Player.GetComponent<Player>().UpdateResource(-ResourceCost);
        }
    }

    public Rect RectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        float x = transform.position.x + transform.anchoredPosition.x;
        float y = Screen.height - transform.position.y - transform.anchoredPosition.y;

        return new Rect(x, y, size.x, size.y);
    }
}
