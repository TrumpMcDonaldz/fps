﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour, IDamageable
{
    public StatMachine SM;

    public Vector2 Health { get; set; }

    public float Damage;

    public float CoolDown;

    private Material Mat;

    public NavMeshAgent NMA;

    public LayerMask TargetLM;

    private void Start()
    {
        Mat = this.transform.GetComponent<MeshRenderer>().material;

        Health = SM.Health;

        Damage = SM.Damage;

        StartCoroutine(AI());

        StartCoroutine(DealDamage());
    }

    public IEnumerator AI()
    {
        if (NMA != default)
        {
            NMA.SetDestination(GameManager.Player.transform.position);
        }

        NMA.speed = 1;

        yield return new WaitForSeconds(1);

        StartCoroutine(AI());   
    }

    public IEnumerator DealDamage()
    {
        Collider[] Cols = Physics.OverlapSphere(this.transform.position, 1.2f / 2, TargetLM);

        if (Cols == default)
        {
            yield return null;

            StartCoroutine(DealDamage());

            yield break;
        }

        foreach (var Col in Cols)
        {
            Player ThePlayer = Col.GetComponent<Player>();

            if (ThePlayer != default)
            {
                ThePlayer.TakeDamage(Damage);

                continue;
            }

            Build TheBuild = Col.GetComponent<Build>();

            if (TheBuild == default)
            {
                continue;
            }

            if (TheBuild is Support)
            {
                continue;
            }

            Col.GetComponent<Build>().TakeDamage(Damage);
        }

        yield return new WaitForSeconds(CoolDown);

        StartCoroutine(DealDamage());
    }

    public void TakeDamage(float Damage)
    {
        Health = new Vector2(Health.x - Damage, Health.y);

        print("Taking Dmg");

        Mat.color = new Color(Mat.color.r, Mat.color.g, Mat.color.b, Health.x / Health.y);

        if (Health.x <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        GameManager.GMComp.UpdateKills(1);

        GameObject Player = GameManager.Player;

        if (Player != default)
        {
            Player.GetComponent<Player>().UpdateResource(10);
        }
    }
}
