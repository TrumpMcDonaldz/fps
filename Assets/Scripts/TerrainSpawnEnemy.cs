﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainSpawnEnemy : MonoBehaviour
{
    public GameObject EnemyPrefab;

    public float SpawnCoolDown;

    void Start()
    {
        StartCoroutine(Spawn());
    }

    public IEnumerator Spawn()
    {
        var TerrainBlocks = new List<Transform>();

        foreach (Transform T in this.transform)
        {
            TerrainBlocks.Add(T);
        }

        Instantiate(EnemyPrefab, TerrainBlocks[Random.Range(0, TerrainBlocks.Count)].position, Quaternion.identity);

        yield return new WaitForSeconds(SpawnCoolDown);

        StartCoroutine(Spawn());
    }
}
