﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class Build : MonoBehaviour, IDamageable
{
    public int BuildRange;

    public LayerMask BuildingLM;

    public List<Build> CheckedForSupport;

    public Support Support;

    public bool IsPreview;

    public Material Mat;

    public MeshRenderer MR;

    public int ResourceCost;

    public GameObject Visualizer;

    public Vector2 Health { get; set; }

    private void Start()
    {
        Health = new Vector2(100, 100);

        Visualizer = GameObject.Find("Visualizer");

        MR = this.GetComponent<MeshRenderer>();

        Mat = MR.material;

        //StartCoroutine(Place());
    }

    public static Vector3 GridPos(Vector3 Pos, int GridSize)
    {
        return new Vector3
            (
            Mathf.RoundToInt(Pos.x / GridSize) * GridSize,
            Mathf.RoundToInt(Pos.y / GridSize) * GridSize,
            Mathf.RoundToInt(Pos.z / GridSize) * GridSize
            );

    }

    public IEnumerator ReschedulePlace(float Seconds)
    {
        yield return new WaitForSeconds(Seconds);

        StartCoroutine(Place());
    }

    public virtual IEnumerator Place()
    {
        yield return null;

        if (!IsPreview)
        {
            StartCoroutine(Place());

            yield break;
        }

        this.GetComponent<Collider>().enabled = false;

        Vector3 PlacePos = default;

        bool CanPlace = false;

        //Check if Building is Placeable

        var Ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (!Physics.Raycast(Ray, out RaycastHit Hit, BuildRange, BuildingLM))
        {
            var PlayerPos = GridPos(GameManager.Player.transform.position, 1);

            var PlayerHeight = GameManager.Player.transform.localScale.y;

            var RotDiff = 90 - Mathf.Abs(Camera.main.transform.rotation.eulerAngles.x - GameManager.Player.transform.rotation.eulerAngles.x);

            //print($"RotDiff - {RotDiff}");

            var ForwardMag = PlayerHeight * Mathf.Tan(RotDiff * Mathf.Deg2Rad);

            ForwardMag = Mathf.Clamp(ForwardMag, 0, 6);

            var Dir = GameManager.Player.transform.TransformDirection(Vector3.forward);

            PlacePos = GridPos(PlayerPos + (ForwardMag * Dir), 1);

            PlacePos.y -= PlayerHeight;

            //print($"PlacePos - {PlacePos}");

            CanPlace = true;

            if (Visualizer != default)
            {
                Visualizer.transform.position = PlacePos;
            }

            if (!Physics.CheckSphere(PlacePos, 1.1f / 2, BuildingLM) || Physics.CheckSphere(PlacePos, 0.1f, BuildingLM))
            {
                PlacePos = default;

                CanPlace = false;
            }
        }
        else
        {
            PlacePos = Hit.collider.gameObject.transform.position + Hit.normal;

            if (Physics.CheckBox(PlacePos, Vector3.one * 0.25f, Quaternion.identity, BuildingLM))
            {
                CanPlace = false;
            }

            else
            {
                CanPlace = true;
            }
        }

        //Resolve Shit

        if (PlacePos == default)
        {
            MR.enabled = false;

            StartCoroutine(Place());

            yield break;
        }

        MR.enabled = true;

        this.transform.position = PlacePos;

        this.transform.rotation = Quaternion.identity;

        if (GameManager.Player.GetComponent<Player>().Resource - ResourceCost < 0)
        {
            CanPlace = false;
        }

        if (!CanPlace)
        {
            //Mat to Red

            var Colour = Color.red;

            Colour.a = 0.5f;

            Mat.color = Colour;

            StartCoroutine(Place());

            yield break;
        }

        //Mat to Blue

        var Colour2 = Color.blue;

        Colour2.a = 0.5f;

        Mat.color = Colour2;

        if (!Input.GetMouseButtonDown(0) && !Input.GetMouseButton(0))
        {
            StartCoroutine(Place());

            yield break;
        }

        //Place the shit.

        GameManager.Player.GetComponent<Player>().UpdateResource(-ResourceCost);

        IsPreview = false;

        Mat.color = Color.cyan;

        this.GetComponent<Collider>().enabled = false;

        var x = Instantiate(this.gameObject, PlacePos, Quaternion.identity);

        x.transform.SetParent(GameManager.Terrain.transform, true);

        x.transform.localScale = Vector3.one;

        IsPreview = true;

        x.GetComponent<Collider>().enabled = true;

        //x.GetComponent<NavMeshObstacle>().enabled = true;

        GameManager.GMComp.UpdateNavMesh();

        StartCoroutine(ReschedulePlace(0.20f));
    }

    public void _InvokeCheckForSupport()
    {
        if (IsPreview)
        {
            return;
        }

        StartCoroutine(InvokeCheckForSupport());
    }

    public IEnumerator InvokeCheckForSupport()
    {
        var SurroundingBuilds = Physics.OverlapSphere(this.transform.position, 0.5f, BuildingLM);

        if (SurroundingBuilds == default)
        {
            yield break;
        }

        Support = default;

        yield return CheckForSupport();

        if (Support == default)
        {
            //Chain Destroy

            StartCoroutine(ChainDestruction(0.25f));
        }
    }

    public IEnumerator CheckForSupport(GameObject ChainStarter = default)
    {
        if (ChainStarter == default)
        {
            ChainStarter = this.gameObject;
        }

        var CS = ChainStarter.GetComponent<Collider>();

        var CSComp = ChainStarter.GetComponent<Build>();

        if (CSComp.Support != default)
        {
            yield break;
        }

        var SurroundingBuilds = Physics.OverlapSphere(this.transform.position, 0.5f, BuildingLM);

        if (SurroundingBuilds == default)
        {
            yield break;
        }

        CSComp.CheckedForSupport.Add(this);

        SurroundingBuilds = SurroundingBuilds.Where(x => !CSComp.CheckedForSupport.Contains(x.GetComponent<Build>()) && x != CS).ToArray();

        foreach (Collider Col in SurroundingBuilds)
        {
            if (Col == default)
            {
                continue;
            }

            var ColComp = Col.GetComponent<Build>();

            if (ColComp is Support)
            {
                CSComp.Support = ColComp as Support;

                yield break;
            }

            yield return StartCoroutine(ColComp.CheckForSupport(ChainStarter));
        }
    }

    public void _ChainDestruction(float DelaySeconds)
    {
        if (IsPreview)
        {
            return;
        }

        StartCoroutine(ChainDestruction(DelaySeconds));
    }

    public IEnumerator ChainDestruction(float DelaySeconds)
    {
        yield return new WaitForSeconds(DelaySeconds);

        Collider[]  SurroundingBuilds = Physics.OverlapSphere(this.transform.position, 0.5f, BuildingLM);

        if (SurroundingBuilds != default)
        {
            foreach (Collider Col in SurroundingBuilds)
            {
                Build Build = Col.GetComponent<Build>();

                if (Build is Support)
                {
                    continue;
                }

                Build._ChainDestruction(DelaySeconds);
            }
        }

        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        var SurroundingBuilds = Physics.OverlapSphere(this.transform.position, 0.5f, BuildingLM);

        if (SurroundingBuilds == default)
        {
            return;
        }

        foreach (Collider Col in SurroundingBuilds)
        {
            Col.GetComponent<Build>()._InvokeCheckForSupport();
        }
    }

    private void OnEnable()
    {
        StartCoroutine(Place());
    }

    public Vector3 DirectionalSnap(Vector3 Dir)
    {
        var x = Dir.x;

        var z = Dir.z;

        //If x is the largest

        if (Mathf.Abs(x) > Mathf.Abs(z))
        {
            return Vector3.Normalize(new Vector3(x, 0, 0));
        }

        else
        {
            return Vector3.Normalize(new Vector3(0, 0, z));
        }
    }

    public void TakeDamage(float Damage)
    {
        Health = new Vector2(Health.x - Damage, Health.y);

        var x = MR.material.color;

        x.a = Health.x / Health.y;

        MR.material.color = x;

        if (Health.x <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
