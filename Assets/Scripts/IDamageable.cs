﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    Vector2 Health { get; set; }

    void TakeDamage(float Damage);
}