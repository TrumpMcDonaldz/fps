﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatMachine
{
    public Vector2 Health;

    public float Damage;

    public float Speed;
}
