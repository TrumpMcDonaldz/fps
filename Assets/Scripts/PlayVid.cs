﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayVid : MonoBehaviour
{
    public VideoPlayer VP;

    public IEnumerator Win()
    {
        VP.Prepare();

        while (!VP.isPrepared)
        {
            yield return null;
        }

        this.GetComponent<RawImage>().texture = VP.texture;

        VP.Play();

        Time.timeScale = 0.1f;

        yield return new WaitForSecondsRealtime(0.5f);

        Time.timeScale = 1f;

        while (VP.isPlaying)
        {
            yield return null;
        }

        SceneManager.LoadScene("Menu");
    }

    public IEnumerator Instructions()
    {
        VP.Prepare();

        while (!VP.isPrepared)
        {
            yield return null;
        }

        this.GetComponent<RawImage>().texture = VP.texture;

        VP.Play();

        Time.timeScale = 0.1f;

        yield return new WaitForSecondsRealtime(0.5f);

        Time.timeScale = 1f;
    }
}
